#Rust Implementation of value

#parameters from table IX in Rust(1987), p.1021
#params=beta,RC,theta11,theta30,theta31

import numpy as np
import matplotlib.pyplot as plt
from scipy.sparse import diags

#params from table IX
params=[.9999,11.7270,4.8259,0.3010,0.6884,0.0106]


maxmil=450000
intervals=5000

#utility function for i=0,1
def utility(x,i,params):
    return -0.001*params[2]*x*(1-i)-params[1]*i

#inner value function to loop over 
def innerval(x,params,EV):
    K=max(EV[0])
    value=np.log((np.exp(utility(x,0,params) + params[0]*EV[0,:] - K) + 
                 np.exp(utility(0,1,params) + params[0]*EV[1,:]) - K)) + K
    return value

#value iteration function
def valueiter(params,maxmil,intervals):
    p=params[-3:-1] #transition probabilities
    K=int(maxmil/intervals)
    P1=diags(p,[0,1],shape=(K,K)).todense()
    P1=P1.T
    x=np.arange(K)
    EV=np.zeros((2,K))
    tol=1e-06;maxIter=10000;iterNum=0;dif=1
    while dif>tol and iterNum<maxIter:
        EV1=innerval(x,params[:3],EV)
        EVTemp=np.vstack((np.dot(EV1,P1),np.array([np.dot(EV1[:2],p)]*90)))
        dif=np.max(abs(EVTemp-EV))
        EV=np.array(EVTemp)
        iterNum+=1
    return EV,iterNum

values,_=valueiter(params,maxmil,intervals)

plt.plot(values[0])
plt.plot(values[1])
plt.show()


#########################################
"""
innerval to be modified for gauss seidel method-->wrong!
modify EVTemp

"""

t=[]
p=params[-3:-1] #transition probabilities
K=int(maxmil/intervals)
P1=diags(p,[0,1],shape=(K,K)).todense()
P1=P1.T
x=np.arange(K)
EV=np.zeros((2,K))
EV1=np.zeros(K)
tol=1e-06;maxIter=10000;iterNum=0;dif=1
while dif>tol and iterNum<maxIter:    
    C=max(EV[0])
    
    EV1=np.log((np.exp(utility(x,0,params) + params[0]*EV[0,:] - C) + 
            np.exp(utility(0,1,params) + params[0]*EV[1,:]) - C)) + C
    EV11=np.zeros_like(EV1)
    for i in range(len(EV11)):
        EV11[i]=np.dot(EV1,P1[:,i])
    EVTemp=np.vstack((EV11,np.array([np.dot(EV1[:2],p)]*90)))
    dif=np.max(abs(EVTemp-EV))
    t.append(dif)
    EV=np.array(EVTemp)
    iterNum+=1
    if iterNum%100==0:
        print(iterNum,dif)
        
plt.plot(EV[0])



diffs=[]
p=params[-3:-1] #transition probabilities
K=int(maxmil/intervals)
P1=diags(p,[0,1],shape=(K,K)).todense()
P1=P1.T
x=np.arange(K)
EV=np.zeros((2,K))
EV1=np.zeros(K)
tol=1e-06;maxIter=10000;iterNum=0;dif=1
while dif>tol and iterNum<maxIter:
    C=max(EV[0])
    EV11=np.zeros_like(EV1)
    for i in range(len(EV11)):
        EV1[i]=np.log((np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C) + 
           np.exp(utility(0,1,params) + params[0]*EV[1,1]) - C)) + C
        EV11[i]=np.dot(EV1,P1[:,i])
    EVTemp=np.vstack((EV11,np.array([np.dot(EV1[:2],p)]*90)))
    dif=np.max(abs(EVTemp-EV))
    diffs.append(dif)
    EV=np.array(EVTemp)
    iterNum+=1
    if iterNum%100==0:
        print(iterNum,dif)

plt.plot(t)
plt.plot(diffs)
plt.show()
np.allclose(EV,values)

"""
bootstrap function
"""

def bootstrap(func,n):
    import time
    
    p=[]

    for i in range(n):
        
        time_start = time.clock()
        y=func(A,x,b)
        time_elapsed = (time.clock() - time_start)
        p.append(time_elapsed)
    
    return np.mean(p),np.sum(p),y

i=0
np.log((np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C) + 
   np.exp(utility(0,1,params) + params[0]*EV[1,1]) - C)) + C

import math
math.log((np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C) + 
   np.exp(utility(0,1,params) + params[0]*EV[1,1]) - C)) + C
          
math.log(np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C))*math.log((np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C) + np.exp(utility(0,1,params) + params[0]*EV[1,1]) - C),np.exp(utility(x[i],0,params) + params[0]*EV[0,i] - C)) + C


import time

p=[]
n=int(1e+06)
for i in range(n):
    
    time_start = time.clock()
    time_elapsed = (time.clock() - time_start)
    p.append(time_elapsed)
print(np.mean(p))

a=2
#0<a<100 is faster with factor ^1 compared to a in range(100)